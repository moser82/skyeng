var tarifData = [
    {
        name: 'mobile',
        description: 'Get notifications about new releases in our mobile app.',
        price: 10,
        img: '/img/device.svg'
    },
    {
        name: 'desktop',
        description: 'Enjoq new episodes on your laptop in browser with our web service, which supports all platforms',
        price: 15,
        img: '/img/laptop.svg'
    },
    {
        name: 'tv',
        description: 'Watch your favourite series at home on large screen with our TV application.',
        price: 20,
        img: '/img/monitor.svg'
    }
]

function loadTarifs() {
    var mainBlock = document.getElementById('main');
    let i = 0;
    tarifData.forEach(tarif => {
        let card = document.createElement('div');
        mainBlock.appendChild(card);
        if (document.documentElement.clientWidth < 721){
            let hr = document.createElement('hr');
            mainBlock.appendChild(hr);
        }
        card.classList.add('card');
        let cardImg = document.createElement('div');
        let cardCaption = document.createElement('div');
        let cardDescription = document.createElement('div');
        let cardPrice = document.createElement('div');
        let img = document.createElement('img');
        card.appendChild(cardImg);
        card.appendChild(cardCaption);
        card.appendChild(cardDescription);
        card.appendChild(cardPrice);
        card.addEventListener('click', showModalDialog(i), false);
        cardImg.classList.add('card__img');
        cardImg.appendChild(img);
        img.setAttribute('src',tarif.img);
        cardCaption.classList.add('card__caption');
        cardCaption.innerHTML = tarif.name;
        cardDescription.classList.add('card__description');
        cardDescription.innerHTML = tarif.description;
        cardPrice.classList.add('card__price');
        cardPrice.innerHTML = tarif.price;
        i++;
    });
}

function hideOverlay() {
    document.getElementById('overlay').style.display = 'none';
}

function showModalDialog(index) {
    return function(tarif){
        var scrollHeight = Math.max(
            document.body.scrollHeight, document.documentElement.scrollHeight,
            document.body.offsetHeight, document.documentElement.offsetHeight,
            document.body.clientHeight, document.documentElement.clientHeight
          );
        tarif = tarifData[index];
        document.getElementById('overlay').style.display = 'block';
        document.getElementById('overlay').style.height = scrollHeight + 'px';
        document.getElementById('modal').style.opacity = 1;
        document.getElementById('modal').style.zIndex = 5;
        if (document.documentElement.clientWidth < 721) {
            document.getElementById('modal').style.top = window.pageYOffset + 'px';
            document.getElementById('modal').style.marginTop = '10vh';
        }        
        document.getElementById('modal-img').setAttribute('src', tarif.img);
        document.getElementById('modal-caption').innerHTML = tarif.name;
        document.getElementById('modal-description').innerHTML = tarif.description;
        document.getElementById('modal-price').innerHTML = tarif.price;
    }    
}

function hideModalDialog() {
    hideOverlay();
    document.getElementById('modal').style.opacity = 0;
    document.getElementById('modal').style.zIndex = -1;
}